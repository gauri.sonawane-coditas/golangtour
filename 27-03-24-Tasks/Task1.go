package main

import "fmt"

/* create the Person class using a struct in Go to represent individuals with
 attributes like name, age, and methods
to introduce themselves, update their age, and check if they are eligible to vote.*/
type person struct {
	name string
	age  int
	gender string
}

func (p person) introduce() {
	fmt.Printf("Hello I am %s and I am %d years old ", p.name, p.age)
}
func (p *person) updateAge(newAge int) int {
	p.age = newAge
	return p.age
}
func canVote(p person) bool {
	if p.age >= 18 {
		return true
	}
	return false
}
func main2() {
	person := person{"Gauri", 21,"female"}
	person.introduce()
	fmt.Println("")
	fmt.Println("persons updated age", person.updateAge(13))
	fmt.Println("Person: ", person)
	fmt.Println("Eligible for voting:", canVote(person))

}
