package main

import (
	"fmt"
	"sync"
)

/* Develop a concurrent CSV file management system in Go, utilizing goroutines and channels, to efficiently 
handle insertion, update, and deletion of records with 1000 data entries while ensuring thread safety and 
optimal performance.*/
func addition(wg *sync.WaitGroup,result chan int,a,b int){
	defer wg.Done()
	result<- a+b
	fmt.Println("addition is",a+b)
	
}
func divide(result chan int,a,b int){
	
	fmt.Println("result is",<-result)
fmt.Println("division is",a/b)
}
func subtract(a,b int){
	fmt.Println("Subtraction is",a-b)
}
func multiply(a,b int){
	res:=a*b
	fmt.Println("Multiplication is",res)
}
func execute(result chan int) {
    wg := new(sync.WaitGroup)
    wg.Add(2)
	go addition(wg,result,10,20)
    // We are increasing the counter by 2
    // because we have 2 goroutines
  
 
    // This Blocks the execution
    // until its counter become 0
    wg.Wait()
}
func main12(){
	
result:=make(chan int)
execute(result)
 divide(result,1,1)
subtract(20,10)
multiply(2,10)

}