package main

import "fmt"
//employee struct
type employee struct {
    name   string
    emp_id int
    salary float64
}
//department struct
type department struct {
    department_name string
    EmpMap          map[string]*employee // Change EmpMap to store pointers to employees
}
//function to raise the salary of employee
func (d *department) raiseSalary(name string) {
    if emp, ok := d.EmpMap[name]; ok {
        emp.salary += 100
        fmt.Println("After raise:", *emp)
    }
}
//function to insert details
func(d *department) insertEmployeedetails(e employee){
	d.EmpMap[e.name]=&e
	

}
func main() {
    Emp := employee{"Gauri", 1, 100000} // Create employee pointer
    EmpMap := map[string]*employee{
        "Gauri": &Emp,
    }
    department := department{"Golang", EmpMap}
    fmt.Println(department)
    department.raiseSalary("Gauri")
    fmt.Println("department-empMap", department.EmpMap)
	
	Emp1:=&employee{"Prahlad",2,2000}
	EmpMap = map[string]*employee{
        "Prahlad": Emp1,
    }
	department.insertEmployeedetails(*Emp1)
	fmt.Println("department",department)

	
}
