package main

import (
	"fmt"
	"log"
)

type employee struct {
	employee_id int
	name        string
	age         int
	salary      int
}

type department struct {
	department_name string
	Employees       []employee
}

func (d *department) insertEmployeedetails(e employee) {
	d.Employees = append(d.Employees, e)
}

func (d *department) deleteEmployeedetails(name string) {
	
	for i, elem := range d.Employees {
		if elem.name == name {
			d.Employees = append(d.Employees[:i], d.Employees[i+1:]...)
			break
		}else{
			log.Fatal("error")
		}
	}
}

func (d *department) giveraisetoEmployee(empname string, raise int) {
	for index, elem := range d.Employees {
		if elem.name == empname {
			elem.salary += raise
			d.Employees[index].salary = elem.salary
		}
	}
	fmt.Printf("salary is raised to %v by %d ",empname,raise)
	fmt.Println("")
}

func (d *department) calculateAvgsalary() int {
	sum := 0
	for _, emp := range d.Employees {
		sum += emp.salary
	}

	if len(d.Employees) == 0 {
		return 0
	}
	return sum / len(d.Employees)
}

func main() {
	department1 := department{
		"Golang",
		[]employee{
			{1001, "ABC", 21, 10000},
			{1002, "XYZ", 22, 20000},
			{1003, "PQR", 24, 30000},
		},
	}
	department2 := department{
		"Data science",
		[]employee{
			{2001, "LMN", 21, 10000},
			{2002, "GHI", 22, 20000},
			{2003, "STU", 24, 30000},
		},
	}

	fmt.Println("Before Inserting:", department1)

	// Inserting a new employee jkl
	employee := employee{1004, "jkl", 21, 12000}
	department1.insertEmployeedetails(employee)
	fmt.Println("After inserting:", department1)

	// Raising the salary by 7000
	department2.giveraisetoEmployee("LMN", 7000)


	// Deleting the employee details
	department1.deleteEmployeedetails("AB")
	fmt.Println("After deleting:", department1)

	// Calculate average salary of department
	avgSalary := department1.calculateAvgsalary()
	fmt.Printf("Average salary is: $%d\n", avgSalary)
}
