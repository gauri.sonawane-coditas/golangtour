package main

import "fmt"

type user struct {
	name string
	Id   int
}
type product struct {
	productId int
	price     int
}

// receiver methods
// value receiver
func (u user) printuserdetails() string {
	return u.name
}
func (p *product) increasePrice(increment int) int {
	return  p.price+increment
}
func main4() {
	user := user{"gauri", 1}
	fmt.Println("", user.printuserdetails())
	product := product{1, 1000}
	productprice:=product.increasePrice(400)
	fmt.Println("product price incremented by",productprice)
	

}