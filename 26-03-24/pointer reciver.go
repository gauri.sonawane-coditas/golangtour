package main
import (
	"fmt"
	"math"
)

type coordinate struct {
	X, Y float64
}

func (v coordinate) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func (v *coordinate) Scale(f float64) {
	v.X = v.X * f
	v.Y = v.Y * f
}

func main() {
	v := coordinate{3, 4}
	v.Scale(10)
	fmt.Println(v.Abs())
}
