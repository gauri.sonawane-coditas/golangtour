package main

import (
	"fmt"
	"math/cmplx"
)
var num int
var (
	ToBe   bool   
    Maxint uint64 = 1<<64-1
	z complex128 = cmplx.Sqrt(-5 + 12i)
)

func main1() {
	fmt.Printf("Type: %T value: %v\n", ToBe,ToBe)//prints default alse value
	fmt.Printf("Type:%T value:%v\n",Maxint,Maxint)
	fmt.Println(num)//it will have zero value
	var i int=32
	var f float64=float64(i)//type conversion
	fmt.Println("f is",f)
	//short hand declaration//type inference
	num1:=121
	fmt.Println("num:",num1)
	const Pi=3.14
	fmt.Println("Pi",Pi)
	//const cant be declared with walrus opeator
}