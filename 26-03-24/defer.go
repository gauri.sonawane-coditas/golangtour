package main

import (
	"fmt"
	"runtime"
	"time"
)

func main2() {
	//evaluate cases from top to bottom
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux")
	default:
		fmt.Printf("%s.\n", os)
	}
	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("Good Morning!")
	case t.Hour() < 17:
		fmt.Println("Good afternoon")
	default:
		fmt.Println("Good evening.")
	}
	defer fmt.Println("world")
	fmt.Println("hello")
	//returns surrounding function first
	for i:=0;i<10;i++{
		defer fmt.Println(i)// last-in-first-out order.
	}
}
