package main

import (
	"log"
	"net/http"
	"time"
)

type result struct {
	url     string
	err     error
	latency time.Duration
}

func get(url string, ch chan<- result) {
	start := time.Now()
	if resp, err := http.Get(url); err != nil {
		ch <- result{}
	} else {
		t := time.Since(start).Round(time.Millisecond)
		ch <- result{url, nil, t}
		resp.Body.Close()
	}
}
func djfnd() {
	results := make(chan result)
	list := []string{"https://amazon.com", "https://google.com", "https://nytimes.com"}
	for _, url := range list {
		go get(url, results)
	}
	for range list {
		r := <-results
		if r.err != nil {
			log.Println(r.url, r.err)
		} else {
			log.Println(r.latency)
		}
	}
}
