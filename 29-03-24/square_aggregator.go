package main

import (
	"fmt"
	"math/rand"
	"sync"
)

func squareWorker(ch chan<- int, list []int, wg *sync.WaitGroup) {
	defer wg.Done()
	sum := 0
	for _, elem := range list {
		sum += elem * elem
	}
	ch <- sum
}

func main() {
	wg := &sync.WaitGroup{}
	mut := &sync.Mutex{}
	var list []int
	var n int //length of list
	var no_of_workers int

	//Taking input
	fmt.Println("Enter the number of records in a list:")
	_, err := fmt.Scanf("%d\n", &n)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	fmt.Println("Enter the number of workers:")
	_, err = fmt.Scanf("%d\n", &no_of_workers)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	// Appending random integers between 0 and n
	for i := 0; i < n; i++ {
		list = append(list, rand.Intn(n))
	}

	//channel
	ch := make(chan int, len(list))

	start := 0
	chunk := len(list) / no_of_workers
	remainder := len(list) % no_of_workers

	for i := 0; i < no_of_workers; i++ {
		wg.Add(1)
		var end int
		if i < remainder {
			end = start + chunk + 1
		} else {
			end = start + chunk
		}
		go squareWorker(ch, list[start:end], wg)
		start = end
	}

	// Close the channel after all workers are done
	go func() {
		wg.Wait()
		close(ch)
	}()

	// Call aggregateSquares after all workers have started
	aggregateSquares(ch, mut)

	wg.Wait()
}

func aggregateSquares(ch <-chan int, mut *sync.Mutex) {
	sum := 0
	mut.Lock()
	for partial_sum := range ch {
		sum += partial_sum
	}
	mut.Unlock()
	fmt.Println("Aggregated sum:", sum)

}
